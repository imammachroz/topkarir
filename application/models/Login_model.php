<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
	public function login($user, $pass) {
		
		$this->db->select('nama, password, "profil1.jpg" as foto, 2 as profil, "Student" as nama_profil');
		$this->db->from('mst_student');
		$this->db->where('nama', $user);
		$this->db->where('password', md5($pass));

		$data1 = $this->db->get_compiled_select();

		$this->db->select('nama, password, "profil1.jpg" as foto, 1 as profil, "Teacher" as nama_profil');
		$this->db->from('mst_teacher');
		$this->db->where('nama', $user);
		$this->db->where('password', md5($pass));
		

		$data2 = $this->db->get_compiled_select();

		$data = $this->db->query($data1." UNION ".$data2);

		if ($data->num_rows() == 1) {
			return $data->row();
		} else {
			return false;
		}
	}

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */