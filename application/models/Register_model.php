<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_model extends CI_Model {
	public function register($table, $data) {
		return $this->db->insert($table, $data);
	}

}

/* End of file Register_model.php */
/* Location: ./application/models/Register_model.php */