<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Login_Controller {
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		
		$data['userdata'] 		= $this->userdata;


		$data['page'] 			= "home";
		$data['judul'] 			= "Beranda";

		$this->template->views('home', $data);
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */