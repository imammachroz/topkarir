<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Register_model');
	}
	
	public function index() {
		$session = $this->session->userdata('status');

		if ($session == '') {
			$this->load->view('register');
		} else {
			redirect('Home');
		}
	}

	public function register() {
		$this->form_validation->set_rules('fullname', 'Nama lengkap', 'required|min_length[4]|max_length[15]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('phone', 'Nomor Telpon', 'required|regex_match[/^[0-9]{10}$/]'); 

		if ($this->form_validation->run() == TRUE) {
			$nama = trim($_POST['fullname']);
			$password = trim($_POST['password']);
			$email = trim($_POST['email']);
			$phone = trim($_POST['phone']);

			$data =[
				'nama' => $nama,
				'password' => md5($password),
				'email' => $email,
				'phone' => $phone
			];

			$insert = $this->Register_model->register("mst_student", $data);

			if ($insert == false) {
				$this->session->set_flashdata('error_msg', 'Ada data Anda Salah.');
				redirect('Register');
			} else {
				echo '<script>alert("Sukses! Anda berhasil melakukan register. Silahkan login untuk mengakses data.");window.location.href="'.base_url('index.php/Login').'";</script>';
			}
		} else {
			$this->session->set_flashdata('error_msg', validation_errors());
			redirect('Register');
		}
	}

	
}

/* End of file Register.php */
/* Location: ./application/controllers/Register.php */