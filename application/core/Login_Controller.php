<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_Controller extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Login_model');

		$this->userdata = $this->session->userdata('userdata');
		
		$this->session->set_flashdata('segment', explode('/', $this->uri->uri_string()));

		if ($this->session->userdata('status') == '') {
			redirect('Login');
		}
	}

	public function updateProfil() {
		if ($this->userdata != '') {
			$data = $this->Login_model->select($this->userdata->id);

			$this->session->set_userdata('userdata', $data);
			$this->userdata = $this->session->userdata('userdata');
		}
	}
}

/* End of file MY_Login.php */
/* Location: ./application/core/MY_Login.php */